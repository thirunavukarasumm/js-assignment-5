// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs


function pairs(obj) {
    if (obj.constructor === Object) {
        let resultArray = [];
        for (key in obj) {
            resultArray.push([key, obj[key]]);
        }
        return resultArray;
    } else {
        return [];
    }
}


module.exports = pairs;
