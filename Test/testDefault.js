const testDefault = require('../defaults')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testObjectWithExtraAttributes = { age: 36, location: 'Gotham', job: "Batman" , powers: "Rich",favouriteVillain: "Joker"};


const resultObject = testDefault(testObject,testObjectWithExtraAttributes);

if (resultObject.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultObject);
}

