const testPairs = require('../pairs')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const resultArray = testPairs(testObject);

if (resultArray.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultArray);
}

