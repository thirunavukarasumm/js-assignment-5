const testInvert = require('../invert')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const resultObject = testInvert(testObject);

if (resultObject.length == 0) {
    console.log("Invalid data was entered");
} else {
    console.log(resultObject);
}

