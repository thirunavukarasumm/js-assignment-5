// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys




function keys(obj) {
    let resultArray = [];
    if (obj.constructor === Object) {
        for (key in obj) {
            resultArray.push(key);
        }
        return resultArray;
    } else if (Array.isArray(obj)) {
        for (index in obj) {
            resultArray.push(String(index));
        }
        return resultArray;
    } else {
        return []
    }


}


module.exports = keys;