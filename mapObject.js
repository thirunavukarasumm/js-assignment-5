// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject



function mapObject(obj, cb) {
    if (obj.constructor === Object) {
        for (key in obj) {
            if (typeof obj[key] == "number" || typeof obj[key] == "string") { // If the value is of any type other than this then it is ignored.
                obj[key] = cb(obj[key], key);
            }

        }
        return obj;
    } else {
        return []
    }
}

module.exports = mapObject;

