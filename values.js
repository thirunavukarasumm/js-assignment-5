// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values


function values(obj) {
    if (obj.constructor === Object) {
        let resultArray = [];
        for (key in obj) {
            if (typeof (obj[key]) != "function"){
                resultArray.push(obj[key]);
            }
        }
        return resultArray;
    } else {
        return []
    }
}


module.exports = values;
