// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults


function defaults(obj, defaultProps) {
    if (obj.constructor === Object) {
        for (key in defaultProps) {
            if (!(key in obj)) {
                obj[key] = defaultProps[key];
            }
        }
        return obj;
    } else {
        return [];
    }
}  


module.exports = defaults;
